﻿using System.Linq;
using Senet.Helpers;

namespace Senet.Handlers
{
    public class StringHandler : BaseHandler
    {
        private const string LanguageKey = "lang";
        private const string LanguageEnd = "end";

        private static StringHandler _handler;

        private StringHandler()
        {
            FileHelper = new FileHelper("stringLibrary.dat");
        }

        public static StringHandler Instance
        {
            get
            {
                if (_handler != null) {return _handler;}
                return (_handler = new StringHandler());
            }
        }

        public string Value(string key)
        {
            return Value(key, string.Empty);
        }

        public string Value(string key, string lang)
        {
            var data = FileHelper.Read();

            if (data == string.Empty){ return ErrorMessage; }

            var dataLines = data.Split('\n');

            key = key.ToLower();

            var currentLang = string.Empty;
            foreach (var dataLine in dataLines)
            {
                var trimmedDataLine = dataLine.TrimEnd('\r');
                if (trimmedDataLine.Count() > 1 && trimmedDataLine[1] == '#')
                {
                    continue; // Comment directive
                }

                if (trimmedDataLine.Contains(':'))
                {
                    currentLang = CurrentLang(trimmedDataLine); // Set language
                }

                var keyValuePair = dataLine.Split('=');

                if (keyValuePair.Count() == 2 && keyValuePair[0].ToLower().Equals(key))
                {
                    if((lang == string.Empty && currentLang ==string.Empty) ||currentLang == lang)
                    {
                        return keyValuePair[1].TrimEnd(new[]{'\r', '\n'});
                    }
                }
            }

            return string.Format(MissingKeyMessage, key);
        }

        private string CurrentLang(string dataLine)
        {
            string currentLang = string.Empty;
            var langDirective = dataLine.Split(':');

            if (langDirective.Count() == 2)
            {
                if (langDirective[0].ToLower() == LanguageKey)
                {
                    if(langDirective[1] != LanguageEnd)
                    {
                        currentLang = langDirective[1]; // Lang directive
                    }
                }
            }
            return currentLang;
        }
    }
}
