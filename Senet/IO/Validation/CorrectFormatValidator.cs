﻿using Senet.Interfaces;

namespace Senet.IO.Validation
{
    class CorrectFormatValidator : IInputValidator
    {
        public bool Validate(char row, string column)
        {
            return char.IsLetter(row) && char.IsDigit(column[0]) && char.IsDigit(column[1]);
        }

        public string ResultMessage()
        {
            return "";
        }      
    }
}
