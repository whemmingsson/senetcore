﻿using System;
using System.Collections.Generic;

namespace Senet.IoC
{
    /// <summary>
    /// IOC Container by Anders Jansson
    /// </summary>
    class Container
    {
        protected Dictionary<string, Func<Container, object>> factories;
        protected Dictionary<string, object> instances;
        protected Dictionary<string, bool> shared;

        public Container()
        {
            this.factories = new Dictionary<string, Func<Container, object>>();
            this.instances = new Dictionary<string, object>();
            this.shared = new Dictionary<string, bool>();
        }

        public void Set(string name, Func<Container, object> factory)
        {
            this.Set(name, factory, true);
        }

        public void Set(string name, Func<Container, object> factory, bool shared)
        {
            this.factories.Add(name, factory);
            this.shared.Add(name, shared);
        }

        public T Get<T>(string name)
        {
            object instance;
            if (!instances.TryGetValue(name, out instance))
            {
                Func<Container, object> factory = null;
                if (!this.factories.TryGetValue(name, out factory))
                {
                    throw new Exception("Could not find factory.");
                }
                instance = factory.DynamicInvoke(new object[] { this });
                var shared = false;
                this.shared.TryGetValue(name, out shared);

                if (shared == true)
                {
                    this.instances.Add(name, instance);
                }
            }
            return (T)instance;
        }
    }
}
