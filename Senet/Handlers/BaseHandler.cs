﻿using Senet.Helpers;

namespace Senet.Handlers
{
    public abstract class BaseHandler
    {
        protected const string ErrorMessage = "Error when reading file";
        protected const string MissingKeyMessage = "Value for key={0} not found";
        protected FileHelper FileHelper;
    }
}
