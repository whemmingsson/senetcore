﻿using System;
using System.Collections.Generic;
using System.Linq;
using Senet.Interfaces;

namespace Senet.AI
{
    /// <summary>
    /// Ranker class, implements the IRanker interface. Uses random selection of move.
    /// </summary>
    public class RandomRanker : IRanker
    {
        /// <summary>
        /// Returns a move from the provied moves based on a random number
        /// </summary>
        /// <param name="moves">All the moves to consider</param>
        /// <param name="path">Full path</param>
        /// <returns>The random generated move</returns>
        public Move Rank(List<Move> moves, string path)
        {
            var r = new Random();
            return moves.ElementAt(r.Next(0, moves.Count()));
        }
    }
}
