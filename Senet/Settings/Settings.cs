﻿using System;
using System.Collections.Generic;
using Senet.Exceptions;
using Senet.Handlers;

namespace Senet.Settings
{
    public class Settings
    {
        private static Settings _instance;

        // SETTING FIELDS
        private string _language;
        private int _boardWidth;
        private int _boardHeight;

        // SETTINGS PROPERTIES
        public int BoardHeight 
        { 
            get 
            {
                if (_boardHeight != int.MinValue)
                {
                    return _boardHeight;
                }

                var value = Value("boardheight");

                if (value == "")
                {
                    throw new SettingNotAvaliableException("The specified setting does not exist in global.settings");
                }

                int result;

                if (int.TryParse(Value("boardheight"), out result))
                {
                    _boardHeight = result;
                    return result;
                }

                throw new TypeMismatchException("Setting value in global.settings did not match specified setting type");
            }
        }

        public int BoardWidth
        {
            get
            {
                if (_boardWidth != int.MinValue)
                {
                    return _boardHeight;
                }

                var value = Value("boardwidth");

                if (value == "")
                {
                    throw new SettingNotAvaliableException("The specified setting does not exist in global.settings");
                }

                int result;
                if (int.TryParse(Value("boardwidth"), out result))
                {
                    return _boardWidth = result;
                }

                throw new TypeMismatchException("Setting value in global.settings did not match specified setting type");
            }
        }
   
        public string Lang 
        {          
            get { return _language ?? (_language = (string)Value("language")); }
        }

        private Settings()
        {
            _boardWidth = int.MinValue;
            _boardHeight = int.MinValue;
        }

        private string Value(string key)
        {
            return SettingsHandler.Instance.Value(key);
        }

        public static Settings Instance
        {
            get
            {
                if (_instance == null)
                {
                    return (_instance = new Settings());
                }

                return _instance;
            }
        }

        public Dictionary<char, int> GetLetterToIntMapper()
        {
            var result = new Dictionary<char, int> { { 'a', 0 }, { 'b', 1 }, { 'c', 2 }, { 'd', 3 }, { 'e', 4 } };
            return result;
        }
    }
}
