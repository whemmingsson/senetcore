﻿using Senet.Helpers;
using Senet.Interfaces;

namespace Senet.IO.Validation
{
    class ValidPositionValidator : IInputValidator
    {
        public bool Validate(char row, string column)
        {
            return BoardHelper.GetPosition(row, column) != null;
        }

        public string ResultMessage()
        {
            return "";
        }
       
    }
}
