﻿
using System.Collections.Generic;
using Senet.Interfaces;

namespace Senet.AI
{
    /// <summary>
    /// Core of the AI
    /// </summary>
    public class AiCore
    {
        private const char AiId = '2';
        private static Board _board;
        private readonly IRanker _ranker;

        /// <summary>
        /// _Constructor
        /// </summary>
        /// <param name="ranker">The ranker to used for desicion making</param>
        /// <param name="board"></param>
        public AiCore(IRanker ranker, Board board = null)
        {
            _ranker = ranker;
            _board = board;
        }

        internal Move SelectMove(int steps)
        {
            var allMoves = new List<Move>();
            var path = _board.Path;

            FillWithMoves(allMoves, path, steps);
            ValidateMoves(allMoves);

            return EvaluateMoves(allMoves, path);
        }

        private void ValidateMoves(List<Move> allMoves)
        {
            for (var i = allMoves.Count - 1; i >= 0; i-- )
            {
                var move = allMoves[i];
                var result = _board.TryMovePiece(move.Start, move.Steps, true);
                if (result != Board.MoveResult.Moved && result != Board.MoveResult.Swapped)
                {
                    allMoves.Remove(move);
                }
                else
                {
                    move.Type = result;
                }
            }
        }

        /// <summary>
        /// Evaluates all the moves provided to the method
        /// </summary>
        /// <param name="allMoves">The moves to evaluate</param>
        /// <param name="path">The board represented as a path</param>
        /// <returns>The best move</returns>
        private Move EvaluateMoves(List<Move> allMoves, string path)
        {
            return _ranker.Rank(allMoves, path);
        }

        private void FillWithMoves(List<Move> allMoves, string path, int steps)
        {
            var counter = 0;
            foreach (var c in path)
            {
                if (c == AiId)
                {
                    allMoves.Add(new Move
                        {
                        StartInPath = counter,
                        EndInPath = counter + steps,
                        Steps = steps,
                        Start = _board.Translate(counter),
                        End = _board.Translate(counter + steps)
                    });
                }
                counter++;
            }
        }      
    }
}
