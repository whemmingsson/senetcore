﻿using System;

namespace Senet
{
    /// <summary>
    /// Dice class that models the special dice used in Senet
    /// </summary>
    static class Dice
    {
        private const int NumberOfSticks = 4;
        private const int FullBlackValue = 6;
        private static readonly Random Random = new Random();

        /// <summary>
        /// Roll the dice
        /// </summary>
        /// <returns>Result object describing the roll outcome</returns>
        public static DiceResult Roll()
        {
            var result = new DiceResult();

            var white = 0;
            var black = 0;

            for (var i = 0; i < NumberOfSticks; i++)
            {
                var color = Random.Next(0, 2);

                if (color == 0) { white++; }
                else { black++; }
            }

            result.Black = black;
            result.White = white;

            return Calc(result); 
        }

        private static DiceResult Calc(DiceResult result)
        {
            if (result.Black == NumberOfSticks)
            {
                result.Total = FullBlackValue;
                result.ThrowAgain = true;
            }
            else if (result.White == NumberOfSticks || result.White == 1)
            {
                result.Total = result.White;
                result.ThrowAgain = true;
            }
            else
            {
                result.Total = result.White;
                result.ThrowAgain = false;
            }

            return result;         
        }
    }


    /// <summary>
    /// Models a Dice Result
    /// </summary>
    public class DiceResult
    {
        /// <summary>
        /// Number of white sides up
        /// </summary>
        public int White { get; set; }

        /// <summary>
        /// Number of black sides up
        /// </summary>
        public int Black { get; set; }

        /// <summary>
        /// Total number of steps (calculated)
        /// </summary>
        public int Total { get; set; }

        /// <summary>
        /// Flag for re-throw
        /// </summary>
        public bool ThrowAgain { get; set; }
    }
}
