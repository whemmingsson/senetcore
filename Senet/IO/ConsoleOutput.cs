﻿using Senet.Interfaces;
using System;

namespace Senet.IO
{
    /// <summary>
    /// Acts as view for the ascii based board that is printed on the screen
    /// </summary>
    class ConsoleOutput : IOutput
   {
       private const char Delimiter = '-';

       /// <summary>
       /// Prints the board to the console
       /// </summary>
       /// <param name="b"></param>
        public void Render(Board b)
        {
            var height = b.Height;
            var width = b.Width;

            PrintDivider(Delimiter, width);
            for (var i = 0; i < height; i++)
            {
                for (var j = 0; j < width; j++)
                {
                    Console.Write(b.Get(j, i));
                    Console.Write(" ");
                }

                Console.WriteLine();
                PrintDivider(Delimiter, width);
            }
        }

        public void Render(string content)
        {
            Console.WriteLine(content);
        }

        public void Clear()
        {
            Console.Clear();
        }

        private void PrintDivider(char character, int length)
        {
            for (var i = 0; i < length; i++) { Console.Write(character); Console.Write(" "); }
            Console.WriteLine();
        }


   }
}
