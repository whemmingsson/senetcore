﻿using System.Globalization;
using Senet.Helpers;
using Senet.Interfaces;

namespace Senet.Validation
{
    class EndPositionNotPieceValidator : IValidator
    {
        public Board.MoveResult Result()
        {
            return Board.MoveResult.Moved;
        }

        bool IValidator.Validate(int start, int end, string path)
        {
            return !BoardHelper.IsPiece(int.Parse(path[end].ToString(CultureInfo.InvariantCulture)));
        }
    }
}
