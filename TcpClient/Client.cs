﻿using System;
using System.Net;
using System.Text;

namespace TcpClient
{
    class Client
    {
        private const int PacketSize =8;
        private readonly ASCIIEncoding _encoder = new ASCIIEncoding();
        private System.Net.Sockets.TcpClient _client;

        public System.Net.Sockets.TcpClient GetClient { get { return _client; } }

        public void Run()
        {
            while(true)
            {
                Console.Write("Enter IP: ");
                var ip = Console.ReadLine();
               _client = new System.Net.Sockets.TcpClient();

                IPEndPoint serverEndPoint;
                try
                {
                    serverEndPoint = new IPEndPoint(IPAddress.Parse(ip), 3000);
                }
                catch (Exception)
                {
                    serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 3000);
                }
            
                _client.Connect(serverEndPoint);

                // READ PLAYER NUMBER
                var message = Read(_client);
                Console.WriteLine(message);

                while (true)
                {
                    message = Read(_client);
                    ParseResult result = Parse(message);
                    if (result.ParseCode == 'M') // The client recieves move from other player. Display it.
                    {
                        Console.WriteLine("Other player moved: " + result.ParseInstruction);

                        // Client should now send own move:
                        Console.Write("Enter move: ");
                        message = Console.ReadLine();
                        Send(_client,message);
                    }

                    if (result.ParseCode == 'I' && result.ParseInstruction == "start")
                    {
                        Console.WriteLine("You are making first move!");
                        // Client should now send own move:
                        Console.Write("Enter move: ");
                        message = Console.ReadLine();
                        Send(_client, message);
                    }

                    if (result.ParseCode == 'W')
                    {
                        Console.WriteLine("Server issued an wait instruction. Please hold.");
                    }

                    if (result.ParseCode == 'D')
                    {
                        Console.WriteLine("Other player disconnected. You have won");
                        Console.ReadKey();
                        System.Environment.Exit(-1);                      
                    }

                }
            }


            Console.ReadKey();

            //var clientStream = client.GetStream();

            //var encoder = new ASCIIEncoding();
            //var buffer = encoder.GetBytes("Hello Server!");

            //clientStream.Write(buffer, 0, buffer.Length);
            //clientStream.Flush();
        }

        private ParseResult Parse(string message)
        {
            var noParseRresult = new ParseResult() {ParseCode = 'N'};
            if (string.IsNullOrEmpty(message) || message.Length != PacketSize)
            {
                return noParseRresult;
            }

            var data = message.Split(':');
            if (data.Length != 2 || data[0].Length != 1 || data[1].Length != 6)
            {
                return noParseRresult;
            }

            var code = data[0];
            var instruction = data[1];

            return new ParseResult()
                {
                    ParseCode = char.Parse(code), 
                    ParseInstruction = data[1].TrimEnd('.'), 
                    ParseMessage = "[Reserved for future use]"
                };
        }

        public void Send(System.Net.Sockets.TcpClient client, string message)
        {
            try
            {
                var clientStream = client.GetStream();
                var buffer = _encoder.GetBytes(message);
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        private string Read(System.Net.Sockets.TcpClient client)
        {
            var stream = client.GetStream();

            var message = new byte[PacketSize];

            while (true)
            {
                if (client.Connected)
                {
                    try
                    {
                        Console.WriteLine("Trying to read...");
                        var bytesRead = stream.Read(message, 0, PacketSize);
                        return _encoder.GetString(message, 0, bytesRead);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                        break;
                    }
                }

                Console.WriteLine("No connection established while reading...");
            }

            return "ERROR";
        }
    }

    internal class ParseResult
    {
        public char ParseCode { set; get; }
        public string ParseInstruction { set; get; }
        public string ParseMessage { set; get; }
    }
}
