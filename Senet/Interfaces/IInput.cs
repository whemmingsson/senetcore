﻿namespace Senet.Interfaces
{
    interface IInput
    {
        /// <summary>
        /// This method MUST return an input of the form "A01" or "a01". Must contain 3 characters where first is letter and second two are numbers 00 - 99.
        /// </summary>
        /// <returns></returns>
        string Read();
    }
}
