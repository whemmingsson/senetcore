﻿namespace Senet.Interfaces
{
    interface IOutput
    {
        void Render(Board board);
        void Render(string content);
        void Clear();
    }
}
