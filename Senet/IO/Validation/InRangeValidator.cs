﻿using Senet.Helpers;
using Senet.Interfaces;

namespace Senet.IO.Validation
{
    class InRangeValidator : IInputValidator
    {
        public int BoardHeight { set; get; }
        public int BoardWidth { set; get; }

        public bool Validate(char row, string column)
        {
            var pos = BoardHelper.GetPosition(row, column);

            if (pos == null) { return false; }

            return pos.Row < BoardHeight && pos.Column < BoardWidth;
        }

        public string ResultMessage()
        {
            return "";
        }

    }
}
