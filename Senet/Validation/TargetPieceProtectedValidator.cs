﻿using Senet.Helpers;
using Senet.Interfaces;
using System;

namespace Senet.Validation
{
    class TargetPieceProtectedValidator : IValidator
    {
        public Board.MoveResult Result()
        {
            return Board.MoveResult.TargetPieceProtected;
        }

        public bool Validate(int start, int end, string path)
        {
            try
            {
                var player = BoardHelper.GetFromPath(end, path);

                if (!BoardHelper.IsPiece(player))
                {
                    return false;
                }
                if (BoardHelper.GetFromPath(end - 1, path) == player || BoardHelper.GetFromPath(end + 1, path) == player)
                {
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                //Debug.WriteLine("CORE ERROR: " + e.Message);
                return false; // This is almost 100% likely in this case, because the error is caused by checking on a square that is outside of board.
            }
        }
    }
}
