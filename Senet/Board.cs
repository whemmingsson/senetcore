﻿using System.Collections.Generic;
using System.Globalization;
using Senet.AI;
using Senet.Helpers;
using Senet.Interfaces;
using Senet.Validation;

namespace Senet
{
    /// <summary>
    /// Internal (non-visual) representation of the board
    /// </summary>
    public class Board
    {
        private const int BoardHeight = 3; // THIS CANNOT BE CHANGED NOT EVEN BY DEVELOPER
        private const int BoardWidth = 10;

        private readonly List<IValidator> _validatorGroup;

        private string _boardRepresentation;
        private bool _boardStatusChanged;

        private readonly int[,] _board;

        public int Height { get { return BoardHeight; } }
        public int Width { get { return BoardWidth; } }

        public string Path
        {
            get
            {
                if (_boardRepresentation != null || _boardStatusChanged)
                {
                    _boardStatusChanged = false; // Reset the flag
                    return _boardRepresentation = GetPath();                   
                }

                return _boardRepresentation;
            }
        }

        public Board()
        {
            _board = new int[BoardWidth, BoardHeight];

            Init();

            _validatorGroup =  new List<IValidator>
                {
                    new SamePlayerValidator(),
                    new TargetPieceBlockedValidator(),
                    new TargetPieceProtectedValidator(),
                    new EndPositionNotPieceValidator()
                };

           
        }

        private void Init()
        {
            for (var i = 0; i < BoardWidth; i++)
            {
                 var playerPiece = i % 2 + 1;
                _board[i, 0] = playerPiece;
                _board[i, 1] = 0;
                _board[i, 2] = 0;
            }
            _boardStatusChanged = true;
        }

        public int Get(int x, int y)
        {
            try{ return int.Parse(_board[x, y].ToString(CultureInfo.InvariantCulture)); }
            catch{ return -1; }
        }

        /// <summary>
        /// USED IN __TESTING__ PURPOSE TO CREATE FAST SETUP
        /// </summary>
        /// <param name="path"></param>
        public void InitFromString(string path)
        {
            if (path.Length != 30) { return; }

            var pos = 0;
            foreach (var c in path)
            {
                AddFromChar(c, pos);
                pos++;
            }

            _boardStatusChanged = true;
        }

        private void AddFromChar(char c, int pos)
        {
            var x = pos % BoardWidth;
            if (pos < BoardWidth)
            {
                _board[x, 0] = int.Parse(c.ToString(CultureInfo.InvariantCulture));
            }
            else if (pos > BoardWidth * 2)
            {
                _board[x, 2] = int.Parse(c.ToString(CultureInfo.InvariantCulture));
            }
            else
            {
                _board[BoardWidth - 1 - x, 1] = int.Parse(c.ToString(CultureInfo.InvariantCulture));
            }
        }

        /// <summary>
        /// Tries to move the given piece on the from position
        /// The parameters comes from the position of the piece that was moved by player/AI, and the number of steps (from dice)
        /// </summary>
        /// <param name="from">Which piece to move</param>
        /// <param name="steps">Number of steps to move</param>
        /// <param name="aiTry">Flag to tell wheater or not to move the piece (players should ignore this or call with false)</param>
        /// <returns>The type of move performed</returns>
        public MoveResult TryMovePiece(Position from, int steps, bool aiTry = false)
        {          
            // TODO: Should this be moved into the validator-group?
            if (!BoardHelper.IsPiece(_board[from.Column, from.Row]))
            {
                return MoveResult.NoPiece;
            }

            var path = Path;// The whole path based on the current board setup

            //Re-evaluate the position and steps into more managable entities
            var startPos = GetPositionInPath(from, 0);
            var endPos = GetPositionInPath(from, steps);

            // Piece is not player piece. Ignore check if AI is calling method.
            if (!aiTry && path[startPos] != '1')
            {
                return MoveResult.NotPlayerPiece;
            }

            var to = Translate(endPos); // USED FOR EASY USE OF THE SWAP AND MOVE METHODS

            // Piece can possibly move out of board
            if (endPos >= BoardHeight*BoardWidth - 1)
            {
                return MoveResult.Special;
            }

            // Run evaluation of move to test its legitimacy
            MoveResult result;
            if((result = Validate(path, startPos, endPos)) != MoveResult.Null)
            {
                if (result == MoveResult.Moved)
                {
                    if (!aiTry) { Move(from, to); }
                }

                return result;
            }

            if (!aiTry) { Swap(from, to); }
            return MoveResult.Swapped;
        }

        private MoveResult Validate(string path, int startPos, int endPos)
        {
            foreach (var validator in _validatorGroup)
            {
                if (validator.Validate(startPos, endPos, path))
                {
                    return validator.Result();
                }
            }
            return MoveResult.Null;
        }

        /// <summary>
        /// Translate from end position (integer) to position object
        /// </summary>
        /// <param name="endPos">Int position to used to translate</param>
        /// <returns>Position object</returns>
        public Position Translate(int endPos)
        {
            var row = endPos / BoardWidth;
            var col = (endPos % BoardWidth);

            if (row == 1)
            {
                col = BoardWidth - (col +1);
            }

            return new Position { Column = col, Row = row };            
        }

        public static int GetPositionInPath(Position from, int steps)
        {
            if (from.Row == 0 || from.Row == 2)
            {
                return from.Row * BoardWidth + from.Column + steps;
            }

            return 2*BoardWidth - from.Column - 1 + steps;
        }

        private void Swap(Position a, Position b)
        {
            var temp = Get(a.Column, a.Row);
            _board[a.Column, a.Row] = Get(b.Column, b.Row);
            _board[b.Column, b.Row] = temp;
            _boardStatusChanged = true;
        }


        private void Move(Position from, Position to)
        {
            _board[to.Column, to.Row] = Get(from.Column, from.Row);
            _board[from.Column, from.Row] = 0;
            _boardStatusChanged = true;
        }

        public void MoveAi(Move move)
        {
            var to = Translate(move.EndInPath);
            var from = Translate(move.StartInPath);

            if (_board[to.Column, to.Row] == 1)
            {
                Swap(from, to);
            }
            else
            {
                Move(from, to);
            }
        }

        private string GetPath()
        {
            var path = string.Empty;
            for (var i = 0; i < BoardHeight; i++)
            {
                if (i != 1)
                {
                    for (var j = 0; j < BoardWidth; j++)
                    {
                         path += _board[j, i];
                    }
                }
                else
                {
                    for (var j = BoardWidth -1; j >= 0 ; j--)
                    {
                         path += _board[j, i];
                    }
                }
            }

            return path;
        }

        public enum MoveResult
        {
            Moved = 0, Swapped, NoPiece, TargetPieceBlocked, TargetPieceProtected, SamePlayer, Special, Null, NotPlayerPiece
        }
    }
}
