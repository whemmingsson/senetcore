﻿using System;
using Senet.IO;
using Senet.Interfaces;
using Senet.IoC;

namespace Senet
{
    class Program
    {
        /// <summary>
        /// Main entry
        /// </summary>
        static void Main()
        {
            var ioc = new Container();

            // IOC registers
            ioc.Set("consoleinput", c => new ConsoleInput());
            ioc.Set("input", c => c.Get<IInput>("consoleinput"));

            var core = new CoreController(ioc.Get<IInput>("input"), new ConsoleOutput());

            // Run the core controller
            core.Run();

            Console.ReadKey();
        }
    }
}
