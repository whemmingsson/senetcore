﻿namespace Senet.AI
{
    /// <summary>
    /// Models a move
    /// </summary>
    public class Move
    {
        /// <summary>
        /// Start position, modeled as an object
        /// </summary>
        public Position Start { set; get; }

        /// <summary>
        /// Start position, modeled as an object
        /// </summary>
        public Position End { set; get; }

        /// <summary>
        /// The position of the start in the whole track
        /// </summary>
        public int StartInPath { set; get; }

        /// <summary>
        /// The position of the end in the whole track
        /// </summary>
        public int EndInPath { set; get; }

        /// <summary>
        /// Number of steps in the move
        /// </summary>
        public int Steps { set; get; }

        /// <summary>
        /// The type (result) of the move
        /// </summary>
        public Senet.Board.MoveResult Type { set; get; }
    }
}
