﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Senet.Interfaces;

namespace Senet.MockClasses
{
    /// <summary>
    /// Mock class used in testing of the Core Controller
    /// </summary>
    class MockInput : IInput
    {
        public string In { get; set; }   
             
        public string Read()
        {
            if (In != null && In.Count() == 2)
            {
                return In.Insert(1, "0");
            }

            return In;
        }
    }
}
