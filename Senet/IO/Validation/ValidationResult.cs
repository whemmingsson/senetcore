﻿namespace Senet.Validation
{
    class ValidationResult
    {
        public bool Result { set; get; }
        public string Message { set; get; }
    }
}
