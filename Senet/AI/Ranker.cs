﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Senet.Interfaces;

namespace Senet.AI
{
    /// <summary>
    /// Real ranker that uses certain weight based algorithms to calculate the best move in the list of given moves
    /// TODO: Use the random ranker while this is not implemented
    /// </summary>
    public class Ranker : IRanker
    {
        private const int BlockadesWeightingFactor = 2;

        public Move Rank(List<Move> moves, string path)
        {
            // Method of ranking:
            /*
             * Create a "before" and "after" paths
             * Count certain aspects like number of "walls" and "protected pieces"
             * Create a rank value based on comparison of the counts, where higher rank means better move
             * Select the highest ranked move and return it
             * */

            foreach (var move in moves)
            {
                var rm  = (RankedMove)move;
                Rank(rm, path);
            }

            return null;
        }

        private void Rank(RankedMove rm, string prePath)
        {
            // Create "postPath"
            var postPath = CreatePostPath(rm, prePath);

            // Get player id (should  be 2)
            var aiPlayer = prePath[rm.StartInPath];

            var rankAspectsBefore = new List<PieceUnit>();
            var rankAspectsAfter = new List<PieceUnit>();

            CountBlockades(prePath, aiPlayer, rankAspectsBefore);
            CountBlockades(postPath, aiPlayer, rankAspectsAfter);

            var rank = CompareAspects(rankAspectsBefore, rankAspectsAfter);
        }

        private int CompareAspects(List<PieceUnit> before, List<PieceUnit> after)
        {
            var rank = 0;

            var blockadesBefore = before.Count;
            var blockadesAfter = after.Count;

            rank += (blockadesAfter - blockadesBefore) * BlockadesWeightingFactor;

            // Other players behind all 
            //var totalOtherPlayersBehindBlockadesBefore = before.Sum(pieceUnit => pieceUnit.OtherPlayersBefore);
            //var totalOtherPlayersBehindBlockadesAfter = after.Sum(pieceUnit => pieceUnit.OtherPlayersBefore);

            //var totalOtherPlayersAheadBlockadesBefore = before.Sum(pieceUnit => pieceUnit.OtherPlayersAfter);
            //var totalOtherPlayersAheadBlockadesAfter = after.Sum(pieceUnit => pieceUnit.OtherPlayersAfter);

            return rank;
        }

        /// <summary>
        /// Public method ONLY used for testing 
        /// </summary>
        /// <param name="prePath"></param>
        /// <param name="player"></param>
        /// <param name="rankAspects"></param>
        public void CountBlockadesTest(string prePath, char player, ICollection<PieceUnit> rankAspects)
        {
            CountBlockades(prePath, player, rankAspects);
        }

        private void CountBlockades(string prePath, char player, ICollection<PieceUnit> rankAspects)
        {
            const int blockadeCount = 3;
            var iterator = 0;
            var pieceCount = 0;
            var otherPlayer = player == '2' ? '1' : '2';
            while (iterator < prePath.Count())
            {
                var current = prePath[iterator];

                // Evaluate piece
                if (current == player)
                {
                    pieceCount++;
                }
                else
                {
                    pieceCount = 0; // Reset counting
                }

                // Blockade found
                if (pieceCount == blockadeCount)
                {
                    var otherPlayerBefore = 0;
                    var otherPlayerAfter = 0;

                    for (var i = 0; i < prePath.Count(); i++)
                    {
                        if (prePath[i] == otherPlayer)
                        {
                            if (i < iterator) // Before
                            {
                                otherPlayerBefore++;
                            }
                            else // After
                            {
                                otherPlayerAfter++;
                            }
                        }
                    }

                    rankAspects.Add(new PieceUnit
                        {
                            Player = player,
                            AdjacentPieces = blockadeCount,
                            OtherPlayersAfter = otherPlayerAfter,
                            OtherPlayersBefore = otherPlayerBefore
                        });

                    pieceCount = 0; // Reset
                }

                iterator++;
            }
        }

        private  string CreatePostPath(Move rm, string prePath)
        {
            var postPath = prePath.Clone().ToString();
            var postPathBuilder = new StringBuilder(postPath);
            var piece = prePath[rm.StartInPath];

            if (prePath[rm.EndInPath] == '0') // Move
            {
                postPathBuilder[rm.EndInPath] = piece;
                postPathBuilder[rm.StartInPath] = '0';
            }
            else // Swap
            {
                var temp = postPathBuilder[rm.EndInPath];
                postPathBuilder[rm.EndInPath] = piece;
                postPathBuilder[rm.StartInPath] = temp;
            }

            return postPathBuilder.ToString();
        }

        public class PieceUnit
        {
            public char Player { get; set; }
            public int AdjacentPieces { get; set; }
            public int OtherPlayersAfter { set; get; }
            public int OtherPlayersBefore { set; get; }
        }
    }

    public class RankedMove : Move
    {
        public int Rank { set; get; }
    }


}
