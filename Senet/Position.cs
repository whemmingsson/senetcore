﻿namespace Senet
{
    /// <summary>
    /// Models a vector
    /// </summary>
    public class Position
    {
        /// <summary>
        /// Row, or Y position
        /// </summary>
        public int Row
        {
            set;
            get;
        }

        /// <summary>
        /// Column, or X position
        /// </summary>
        public int Column
        {
            set;
            get;
        }
    }
}
