﻿using Senet.Interfaces;
using System;
using System.Linq;

namespace Senet.IO
{
    class ConsoleInput : IInput
    {
        public string Read()
        {
            var line = Console.ReadLine();

            if (line != null && line.Count() == 2)
            {
                return line.Insert(1, "0");
            }

            return line;
        }

        public void SetManager(object manager)
        {
            throw new NotImplementedException();
        }
    }
}
