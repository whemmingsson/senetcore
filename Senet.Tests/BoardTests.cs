﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Senet;

namespace Senet.Tests
{
    [TestClass]
    public class BoardTests
    {
        private Board _board;
        private string initPath;

        [TestInitialize]
        public void TestInitialize()
        {
            _board = new Board();
            initPath = "121212121200000000000000000000";
            _board.InitFromString(initPath);
        }

        private void InitBoard(string path)
        {
            if (path.Length < 30)
            {
                for (var i = path.Length; i < 30; i++) { path += '0'; }
            }

            _board.InitFromString(path);
        }

        [TestMethod]
        public void BoardInit_Test()
        {
            Board board = new Board();
            Assert.IsNotNull(board);
        }

        [TestMethod]
        public void GetPath_Test()
        {
            Board board = new Board();
            string path = "121212121200000000000000000000";

            Assert.AreEqual(path, board.Path);
        }

        [TestMethod]
        public void TestInit_Test()
        {
             Board board = new Board();
             string path = "121212121200000000000000000000";

             board.InitFromString(path);

             Assert.AreEqual(path, board.Path);
        }

        [TestMethod]
        public void MovePiece_Test()
        {
            Position p = new Position() { Row = 0, Column = 8 };
            var steps = 2;
            string newPath = "121212120210000000000000000000";

            Board.MoveResult mr = _board.TryMovePiece(p, steps);

            Assert.AreEqual(Board.MoveResult.Moved, mr);
            Assert.AreEqual(newPath, _board.Path);
        }

        [TestMethod]
        public void SwapPiece_Test()
        {
            Position p = new Position() { Row = 0, Column = 8 };
            var steps = 1;
            string newPath = "121212122100000000000000000000";

            Board.MoveResult mr = _board.TryMovePiece(p, steps);

            Assert.AreEqual(Board.MoveResult.Swapped, mr);
            Assert.AreEqual(newPath, _board.Path);
        }

        [TestMethod]
        public void PieceProtected_Test()
        {
            var initPath = "122";
            Position p = new Position() { Row = 0, Column = 0 };
            var steps = 1;
            InitBoard(initPath);

            Board.MoveResult mr = _board.TryMovePiece(p, steps);

            Assert.AreEqual(Board.MoveResult.TargetPieceProtected, mr);
            Assert.AreEqual(initPath, _board.Path.Substring(0, 3));
        }

        [TestMethod]
        public void PieceBlocked_Test()
        {
            var initPath = "12220";
            Position p = new Position() { Row = 0, Column = 0 };
            var steps = 4;
            InitBoard(initPath);

            Board.MoveResult mr = _board.TryMovePiece(p, steps);

            Assert.AreEqual(Board.MoveResult.TargetPieceBlocked, mr);
            Assert.AreEqual(initPath, _board.Path.Substring(0, 5));
        }
    }
}
