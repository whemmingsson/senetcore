﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SenetGUI
{
    class StateButton : Button
    {
        public State ButtonState { set; get; }
        public bool Selected { set; get; }

        public enum State
        {
            AiPiece, PlayerPiece, Special
        }
    }  
}
