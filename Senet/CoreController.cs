﻿using System.Globalization;
using System.Linq;
using Senet.AI;
using Senet.Helpers;
using Senet.IO.Validation;
using Senet.Interfaces;
using System.Collections.Generic;
using System.Diagnostics;

//using Senet.AI;

namespace Senet
{
    /// <summary>
    /// Acts as the "glue" between the internal board and the graphical output
    /// Call diagram:   Mapper -> BoardView(Board data)
    /// </summary>
    internal class CoreController : Base
    {
        private readonly Board _board;
        private readonly IInput _input;
        private readonly IOutput _output;
        private readonly AiCore _ai;

        public CoreController(IInput input, IOutput output)
        {
            _board = new Board();
            _input = input;
            _output = output;
            _ai = new AiCore(new RandomRanker(), _board); // Use the random ranker
        }

        /// <summary>
        /// Renders the board
        /// </summary>
        private void Render()
        {
            _output.Render(_board);
        }

        /// <summary>
        /// Renders a string
        /// </summary>
        /// <param name="message"></param>
        private void Render(string message)
        {
            _output.Render(message);
        }

        /// <summary>
        /// Reads from the user input
        /// </summary>
        /// <returns></returns>
        private string Read()
        {
            return _input.Read();
        }

        /// <summary>
        /// Returns a list of all input validators
        /// </summary>
        /// <returns></returns>
        private IEnumerable<IInputValidator> GetInputValidators()
        {
            return new List<IInputValidator>
                {
                    new CorrectFormatValidator(),
                    new ValidPositionValidator(),
                    new InRangeValidator{BoardHeight = _board.Height, BoardWidth = _board.Width}
                };
        }

        /// <summary>
        /// Validates the input and calls the board to validate move
        /// </summary>
        /// <param name="input">The input from the user</param>
        /// <param name="steps">Number of steps to move (from dice)</param>
        /// <returns>True if move was performed, false if not</returns>
        private bool TranslateAndTryMove(string input, int steps)
        {
            // Input should be of form "A1" or "C5"
            if (input.Length != 3)
            {
                return false;
            }

            var row = input.Substring(0, 1)[0];
            var col = input.Substring(1, 2);

            if (GetInputValidators().Any(inputValidator => !inputValidator.Validate(row, col)))
            {
                Render("Invalid move!");
                return false;
            }

            var res = _board.TryMovePiece(BoardHelper.GetPosition(row, col), steps); // Main call to board

            if (res != Board.MoveResult.Moved && res != Board.MoveResult.Swapped)
            {
                Render("Invalid move: " + GetMessage(res));
                return false;
            }

            Render("Valid move: " + GetMessage(res));
            return true;
        }

        /// <summary>
        /// Get message to display
        /// </summary>
        /// <param name="res"></param>
        /// <returns></returns>
        private string GetMessage(Board.MoveResult res)
        {
            var mapper = new Dictionary<Board.MoveResult, string>
                 {
                    {Board.MoveResult.SamePlayer, Text("sameplayer")},
                    {Board.MoveResult.Swapped, Text(("swapped"))},
                    {Board.MoveResult.Moved, Text("moved")},
                    {Board.MoveResult.TargetPieceBlocked, Text("targetblocked")},
                    {Board.MoveResult.TargetPieceProtected, Text("targetprotected")},
                    {Board.MoveResult.NotPlayerPiece, Text("notplayerpiece")},
                    {Board.MoveResult.NoPiece, Text("nopiece")}
                };

            string result;
            return mapper.TryGetValue(res, out result) ? result : "";
        }


        /// <summary>
        /// MAIN METHOD
        /// </summary>
        public void Run()
        {
            while (true)
            {               
               RunPlayer();
               RunAi();
            }
        }

        /// <summary>
        /// Run the AI's operations
        /// </summary>
        private void RunAi()
        {
            var aiReThrow = true;
            while (aiReThrow)
            {
                var dr = Dice.Roll();
                aiReThrow = dr.ThrowAgain;

                Render("AI throws:");
                Render("White: " + dr.White + "    Black: " + dr.Black + "    Total: " + dr.Total);
                Render(dr.ThrowAgain ? Text("reroll") : "");

                var move = _ai.SelectMove(dr.Total);
                if (move != null)
                {
                    _board.MoveAi(move);
                    Render(Text("moved"));
                    Render("Ai moves:" + move.Start.Row.ToString(CultureInfo.InvariantCulture) + "_" + move.Start.Column.ToString(CultureInfo.InvariantCulture) + " to " +
                           move.End.Row.ToString(CultureInfo.InvariantCulture) + "_" + move.End.Column.ToString(CultureInfo.InvariantCulture));
                }
                else
                {
                    Render("GENERAL ERROR: AI cannot move any piece");
                }
           }
        }

        /// <summary>
        /// Run the players operations
        /// </summary>
        private void RunPlayer()
        {
            var playerReThrow = true;
     
            var i = 0;
            while (playerReThrow)
            {
                Render();

                var dr = Dice.Roll();
                playerReThrow = dr.ThrowAgain;

                if (i > 0)
                {
                    Render(Text("rerollintro") + " [" + i + "]");
                }

                Render("White: " + dr.White + "    Black: " + dr.Black + "    Total: " + dr.Total);
                Render(dr.ThrowAgain ? Text("reroll") : "");

                var moved = false;
                while(!moved)
                {
                    Render(Text("selectpiece") + ":");
                    var input = Read();

                    moved = TranslateAndTryMove(input, dr.Total);
                    Render(moved ? Text("moved") : Text("notmoved"));
                }
                i++;
            }
        }
    }
}
