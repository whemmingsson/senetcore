﻿using Senet.Handlers;

namespace Senet
{
    /// <summary>
    /// Base-class used mainly for string management
    /// </summary>
    class Base
    {
        /// <summary>
        /// Returns a string from the string library using the current default language
        /// </summary>
        /// <param name="key">Key used in the library</param>
        /// <returns>The translation for the current default language</returns>
        public string Text(string key)
        {
            var lang = Settings.Settings.Instance.Lang;
            return StringHandler.Instance.Value(key, lang);
        }

        /// <summary>
        /// Returns a translated string from the library 
        /// </summary>
        /// <param name="key">Key used in library</param>
        /// <param name="lang">Language selector</param>
        /// <returns></returns>
        public string Text(string key, string lang)
        {
            return StringHandler.Instance.Value(key, lang);
        }
    }
}
