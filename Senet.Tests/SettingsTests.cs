﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Senet.Tests
{
    [TestClass]
    public class SettingsTests
    {
        [TestMethod]
        public void Language_Test()
        {
            var lang = Settings.Settings.Instance.Lang;
            Assert.AreEqual("en", lang);
        }

        [TestMethod]
        public void NullableInt_Test()
        {
            var height = Settings.Settings.Instance.BoardHeight;
            Assert.AreEqual(3, height);
        }
    }
}
