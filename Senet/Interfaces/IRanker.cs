﻿using System.Collections.Generic;
using Senet.AI;

namespace Senet.Interfaces
{
    public interface IRanker
    {
        Move Rank(List<Move> moves, string path);
    }
}
