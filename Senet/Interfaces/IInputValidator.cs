﻿namespace Senet.Interfaces
{
    interface IInputValidator
    {
         bool Validate(char row, string column);
         string ResultMessage();
    }
}
