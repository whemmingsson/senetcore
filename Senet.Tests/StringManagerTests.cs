﻿using System;
using Senet;
using Senet.Handlers;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Senet.Tests
{
    /// <summary>
    /// NOTE THAT THIS CLASS USES FILESYSTEM, AND SHOULD _NOT_ BE CALLED UNIT TEST!!!
    /// </summary>
    [TestClass]
    public class StringManagerTests
    {
        [TestMethod]
        public void Language_Test()
        {
            string key = "language";
            string langKey = "en";
            
            string value = StringHandler.Instance.Value(key, langKey);

            Assert.AreEqual("english", value);
        }

        [TestMethod]
        public void OtherLanguage_Test()
        {
            string key = "language";
            string langKey = "sv";

            string value = StringHandler.Instance.Value(key, langKey);

            Assert.AreEqual("svenska", value);
        }

        [TestMethod]
        public void DefaultLanguage_Test()
        {
            string key = "language";

            string value = StringHandler.Instance.Value(key);

            Assert.AreEqual("nolang", value);
        }
    }
}
