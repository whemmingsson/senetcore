﻿using System;

namespace Senet.Exceptions
{
    class SettingNotAvaliableException : Exception
    {
        public SettingNotAvaliableException(string message)
            : base(message)
        {
        }
    }
}
