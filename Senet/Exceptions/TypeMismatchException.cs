﻿using System;

namespace Senet.Exceptions
{
    class TypeMismatchException : Exception
    {
        public TypeMismatchException(string message) : base(message)
        {
        }
    }   
}
