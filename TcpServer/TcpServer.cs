﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace TcpServer
{
    class TcpServer
    {
        //CONFIG
        private const int Port = 3000;
        private const int PacketSize = 8; // ACCORDING TO PROTOCOL

        private readonly TcpListener _tcpListener;
        private readonly Thread _listenThread;
        private readonly ASCIIEncoding _encoder;

        public TcpServer()
        {
            _tcpListener = new TcpListener(IPAddress.Any, Port);
            _listenThread = new Thread(Listen);
            _encoder = new ASCIIEncoding();
            _listenThread.Start();
        }

        private void Listen()
        {
            Console.WriteLine("Initiating listening...");
            Debug.WriteLine("Listening...");
            _tcpListener.Start();

            try
            {
                while (true)
                {
                    var clients = new TcpClient[2];

                    Console.WriteLine("Listening...");
                    clients[0] = _tcpListener.AcceptTcpClient();
                    Console.WriteLine("Connection established to client 1...");
                    clients[1] = _tcpListener.AcceptTcpClient();
                    Console.WriteLine("Connection established to client 2...");

                    try
                    {
                        HandleClientComm(clients); // No threading
                    }
                    catch (Exception)
                    {
                        if (!clients[0].Connected && clients[1].Connected)
                        {
                            Send(clients[1], "D:pldc..");
                        }
                        else
                        {
                            Send(clients[0], "D:pldc..");
                        }

                        Console.WriteLine("Client disconnected...");
                        Console.WriteLine("Press any key to terminate server");
                        Console.ReadKey();
                    }                  
                }
            }
            catch (Exception e)
            {
               Console.WriteLine("Error in server: "  + e.Message);
            }
            
        }

        private void Send(TcpClient client, string message)
        {
            var clientStream = client.GetStream();
            var buffer = _encoder.GetBytes(message);
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();
        }

        private string Read(TcpClient client)
        {
            var stream = client.GetStream();

            var message = new byte[PacketSize];

            while (true)
            {
                if (client.Connected)
                {
                    var bytesRead = 0;
                    try
                    {
                        Console.WriteLine("Trying to read...");
                        bytesRead = stream.Read(message, 0, PacketSize);
                        return _encoder.GetString(message, 0, bytesRead);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                        break;
                    }
                }
                else
                {
                    Console.WriteLine("No connection established while reading...");
                }
            }

            return "ERROR";
        }

        private void HandleClientComm(TcpClient[] clients)
        {
            // STEP ONE - TELL THE PLAYERS WHAT ORDER THEY SHOULD PLAY
            Send(clients[0], "I:play1.");
            Send(clients[1], "I:play2.");


            Send(clients[0], "I:start.");
            // Clients are now aware of which order they should play

            while(true)
            {
                // STEP TWO - READ MOVE FROM PLAYER 1 and SEND TO PLAYER 2
                Console.WriteLine("Player 1 to Player 2");
                string move = Read(clients[0]);

                Send(clients[1], move);
                if (move[0] == 'D')
                {
                    Console.WriteLine("Client disconnected...");
                }
               
                // STEP THREE - READ MOVE FROM PLAYER 2 and SEND TO PLAYER 1
                Console.WriteLine("Player 2 to Player 1");
                move = Read(clients[1]);
                Send(clients[0], move);

                if (move[0] == 'D')
                {
                    Console.WriteLine("Client disconnected...");
                }
            }
        }
    }
}
