﻿using System.Linq;
using Senet.Helpers;

namespace Senet.Handlers
{
    class SettingsHandler : BaseHandler
    {
        private static SettingsHandler _handler;
        private new const string MissingKeyMessage = "";

        private SettingsHandler()
        {
            FileHelper = new FileHelper("global.settings");
        }

        public static SettingsHandler Instance
        {
            get
            {
                if (_handler != null)
                {
                    return _handler;
                }

                return (_handler = new SettingsHandler());
            }
        }

        public string Value(string key)
        {
            var data = FileHelper.Read();

            if (data == string.Empty)
            {
                return ErrorMessage;
            }

            var dataLines = data.Replace("\r", "").Split('\n');

            key = key.ToLower();

            foreach (var dataLine in dataLines)
            {
                if (IsComment(dataLine))
                {
                    continue; // Comment directive
                }

                var keyValuePair = dataLine.Split('=');

                if (IsValue(key, keyValuePair))
                {
                    return TrimEnd(keyValuePair[1]);
                }
            }

            return MissingKeyMessage;
        }

        private static string TrimEnd(string value)
        {
            return value.TrimEnd(new[]{'\r', '\n'});
        }

        private static bool IsValue(string key, string[] keyValuePair)
        {
            return keyValuePair.Count() == 2 && keyValuePair[0].ToLower().Equals(key);
        }

        private static bool IsComment(string dataLine)
        {
            return dataLine.Count() > 1 && dataLine[1] == '#';
        }
    }
}
