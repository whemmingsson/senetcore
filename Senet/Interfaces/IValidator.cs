﻿namespace Senet.Interfaces
{
    public interface IValidator
    {
        bool Validate(int start, int end, string path);
        Board.MoveResult Result();
    }
}
