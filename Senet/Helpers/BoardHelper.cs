﻿using System;
using System.Diagnostics;
using System.Globalization;

namespace Senet.Helpers
{
    /// <summary>
    /// Helper class for board checks
    /// NOTE: Functionality moved to special validators instead
    /// </summary>
    static class BoardHelper
    {
        /// <summary>
        /// Get the piece at position in path
        /// </summary>
        /// <param name="position"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static int GetFromPath(int position, string path)
        {
            try
            {
                return int.Parse(path[position].ToString(CultureInfo.InvariantCulture));
            }
            catch
            {
                return -1;
            }
        }

        /// <summary>
        /// Validates the position read from the user
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public static Position GetPosition(char row, string col)
        {
            int rowNr;
            int colNr;

            var mapper = Settings.Settings.Instance.GetLetterToIntMapper();

            if (!int.TryParse(col.ToString(CultureInfo.InvariantCulture), out colNr))
            {
                return null;
            }

            return !mapper.TryGetValue(char.ToLower(row), out rowNr) ? null : new Position { Row = rowNr, Column = colNr };
        }

        [Obsolete("Method not used at 2013-09-34 [commit 06b665d]")]
        public static bool IsStart(Position from, int i, int j)
        {
            return i == from.Column && j == from.Row;
        }

        public static bool IsPiece(int value)
        {
            return value > 0 && value <= 2;
        }

        [Obsolete("Method not used at 2013-09-34 [commit 06b665d]")]
        public static bool IsBlocked(int startpos, int endPos, string path)
        {
            const int blockSize = 3;
            var count = 0;
            var i = endPos;
            var player = GetFromPath(startpos, path);
            var otherPlayer = player == 1 ? 2 : 1;

            while (count < blockSize && i < path.Length && i > startpos)
            {
                count = GetFromPath(i, path) == otherPlayer ? count + 1 : 0;
                i--;
            }

            return count >= blockSize;
        }

        [Obsolete("Method not used at 2013-09-34 [commit 06b665d]")]
        public static bool IsProtected(int endPos, string path)
        {
            try
            {
                var player = GetFromPath(endPos, path);

                if (!IsPiece(player))
                {
                    return false;
                }
                return GetFromPath(endPos - 1, path) == player || GetFromPath(endPos + 1, path) == player;
            }
            catch (Exception e)
            {
                Debug.WriteLine("CORE ERROR: " + e.Message);
                return false; // This is almost 100% likely in this case, because the error is caused by checking on a square that is outside of board.
            }
        }
    }
}
