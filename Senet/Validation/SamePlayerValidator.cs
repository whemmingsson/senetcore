﻿using Senet.Interfaces;
using System;

namespace Senet.Validation
{
    class SamePlayerValidator : IValidator
    {
        public Board.MoveResult Result()
        {
            return Board.MoveResult.SamePlayer;
        }

        public Board.MoveResult Validate(int start, int end, string path)
        {
            throw new NotImplementedException();
        }

        bool IValidator.Validate(int start, int end, string path)
        {
            return path[start] == path[end];
        }
    }
}
