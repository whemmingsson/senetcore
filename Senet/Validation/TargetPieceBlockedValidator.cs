﻿using Senet.Helpers;
using Senet.Interfaces;

namespace Senet.Validation
{
    class TargetPieceBlockedValidator : IValidator
    {
        public Board.MoveResult Result()
        {
            return Board.MoveResult.TargetPieceBlocked;
        }

        bool IValidator.Validate(int start, int end, string path)
        {
            const int blockSize = 3;
            var count = 0;
            var i = end;
            var player = BoardHelper.GetFromPath(start, path);
            var otherPlayer = player == 1 ? 2 : 1;

            while (count < blockSize && i < path.Length && i > start)
            {
                if (BoardHelper.GetFromPath(i, path) == otherPlayer)
                {
                    count++;
                }
                else
                {
                    count = 0;
                }

                i--;
            }

            return count >= blockSize;
        }
    }
}
