﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SenetGUI
{
    public partial class SenetGuiForm : Form
    {
        private List<StateButton> _boardButtons;
        private bool _pieceSelected;
        private StateButton _selected;

        private const string AiName = "AI";
        private const string PlayerName = "P";

        private const int ButtonSize = 50;

        public SenetGuiForm()
        {
            InitializeComponent();
        }

        private void SenetGUIForm_Load(object sender, EventArgs e)
        {
            _pieceSelected = false;
            _selected = null;
            _boardButtons = new List<StateButton>();
            CreateBoardButtons();
        }

        private void CreateBoardButtons()
        {
            for (int i = 0; i < 10; i++)
            {
                var button = new StateButton {ButtonState = StateButton.State.Special, FlatStyle = FlatStyle.Flat};

                if (i < 5)
                {
                    button.ButtonState = i%2 == 0 ? StateButton.State.AiPiece : StateButton.State.PlayerPiece;
                    button.BackColor = i%2 == 0 ? Color.Salmon : Color.GreenYellow;
                    button.Text = i % 2 == 0 ? AiName : PlayerName;
                }

                button.Selected = false;
                button.FlatStyle = FlatStyle.Flat;

                button.Width = ButtonSize;
                button.Height = ButtonSize;
                button.Top = ButtonSize;
                button.Left = (ButtonSize + 5) * i + ButtonSize;
                button.Click += button_Click;
                _boardButtons.Add(button);

            }

            this.Controls.AddRange(_boardButtons.ToArray<StateButton>());
        }

        void button_Click(object sender, EventArgs e)
        {
            var btn = sender as StateButton;
      
            if (btn.ButtonState == StateButton.State.Special)
            {
                return;
            }

            if (!_pieceSelected && btn.Text == PlayerName)
            {
                Select(btn);
                //UnselectButtons(btn);

                // The button has been pressed and the move is selected
                // Now, the move should be run trough the game engine to verify that it's a legal move
                // The engine (or core) expects the input to be at the specific format, namely as follows:
                // A05 or B09.
            }
            else if(!_pieceSelected && btn.Text == AiName)
            {
                MessageBox.Show("This is an AI piece. You cannot move it.");
            }
            else
            {
                if (btn.Equals(_selected))
                {
                    Unselect(btn);
                }
                else
                {
                  
                }

            }
          
        }

        private void UnselectButtons(StateButton currentBtn)
        {
            foreach (var boardButton in _boardButtons)
            {
                if (boardButton != currentBtn)
                {
                   Unselect(boardButton);
                }
            }
        }

        private void Select(StateButton button)
        {
            button.FlatAppearance.BorderSize = 5;
            button.FlatAppearance.BorderColor = Color.Indigo;
            button.Selected = true;

            _selected = button;
            _pieceSelected = true;
        }

        private void Unselect(StateButton button)
        {
            button.Selected = false;
            button.FlatAppearance.BorderSize = 1;
            button.FlatAppearance.BorderColor = Color.Black;

            _selected = null;
            _pieceSelected = false;
        }
    }
}
